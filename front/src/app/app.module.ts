import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppComponent } from './app.component';
import { AppRoutingModule } from './app-routing.module';
import { HTTP_INTERCEPTORS, HttpClientModule } from '@angular/common/http';
import { AccueilComponent } from './pages/accueil/accueil.component';
import { ProjectDetailComponent } from './pages/project-detail/project-detail.component';
import { NewProjectComponent } from './pages/new-project/new-project.component';
import { FieldModule } from "./components/field/field.module";
import { ReactiveFormsModule } from '@angular/forms';
import { NewTicketComponent } from './pages/new-ticket/new-ticket.component';
import { TicketComponent } from './components/ticket/ticket.component';
import { UpdateProjectComponent } from './pages/update-project/update-project.component';
import { UpdateTicketComponent } from './pages/update-ticket/update-ticket.component';
import { LoginComponent } from './components/login/login.component';
import { LoginModule } from './components/login/login.module';
import { BasicAuthInterceptor } from './helpers/basic-auth.interceptor';
import { ErrorInterceptor } from './helpers/error.interceptor';

@NgModule({
    declarations: [
        AppComponent,
        AccueilComponent,
        ProjectDetailComponent,
        NewProjectComponent,
        NewTicketComponent,
        TicketComponent,
        UpdateProjectComponent,
        UpdateTicketComponent,
        
    ],
    providers: [
        {
            provide: HTTP_INTERCEPTORS, useClass: BasicAuthInterceptor,
            multi: true
        },
        {
            provide: HTTP_INTERCEPTORS, useClass: ErrorInterceptor,
            multi: true
        }
    ],
    bootstrap: [AppComponent],
    imports: [
        BrowserModule,
        AppRoutingModule,
        HttpClientModule,
        FieldModule,
        ReactiveFormsModule,
        LoginModule
    ]
})
export class AppModule { }

