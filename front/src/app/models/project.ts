import { Ticket } from "./ticket";

export interface Project {
    id: string;
    name: string;
    description: string;
    createdAt: string;
    ticketsList: [Ticket]
}
