export interface Ticket {
    id: string;
    title: string;
    content: string;
    created_at: string;
    status: string;
    projectId: number;
}

