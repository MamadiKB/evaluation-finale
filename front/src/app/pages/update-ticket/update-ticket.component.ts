import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { Ticket } from 'src/app/models/ticket';
import { TicketService } from 'src/app/service/ticket.service';

@Component({
  selector: 'app-update-ticket',
  templateUrl: './update-ticket.component.html',
  styleUrls: ['./update-ticket.component.css']
})
export class UpdateTicketComponent implements OnInit {

  projectId!: string;

  ticketId!: string;


  constructor(private _ticketService:TicketService,  private _router: Router, private _route: ActivatedRoute ) { }

  ngOnInit(): void {
    this.projectId = this._route.snapshot.paramMap.get('projectId') as string;
    this.ticketId = this._route.snapshot.paramMap.get('ticketId') as string;
    this._ticketService.getTicketById(this.ticketId).subscribe((ticket) => {
      if (ticket) {
        this.myForm.patchValue({
          title: ticket.title,
          content: ticket.content,
          status: ticket.status
        })
      }
    })
  }

  myForm = new FormGroup({
    title: new FormControl('', [Validators.required]),
    content: new FormControl('', [Validators.required]),
    status: new FormControl('', [Validators.required]),
  })

  update():void {    
    if (this.myForm.valid) {
      
      const values = this.myForm.value;
      const result = {...values} as unknown as Ticket;
      this._ticketService.updateTicket(result,this.ticketId).subscribe(() => {
      
        const currentUrl = this._router.url;
        this._router.navigateByUrl('/', {skipLocationChange: true}).then(() => {
          this._router.navigate(['/detail/' + this.projectId]);
        });
      });
    };
  };

}
