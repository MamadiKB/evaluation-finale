import { Component, OnInit } from '@angular/core';
import { Project } from 'src/app/models/project';
import { ProjectService } from 'src/app/service/project.service';

@Component({
  selector: 'app-accueil',
  templateUrl: './accueil.component.html',
  styleUrls: ['./accueil.component.css']
})
export class AccueilComponent implements OnInit {

  projectsArray: Project[] = []

  constructor(private _projectService: ProjectService) { }

  ngOnInit(): void {
    this._projectService.getAll().subscribe( project => {
      this.projectsArray.push(...project)
    } )

    console.log(this.projectsArray);
    
  }
}
