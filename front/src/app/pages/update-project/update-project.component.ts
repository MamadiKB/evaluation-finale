import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { Project } from 'src/app/models/project';
import { ProjectService } from 'src/app/service/project.service';

@Component({
  selector: 'app-update-project',
  templateUrl: './update-project.component.html',
  styleUrls: ['./update-project.component.css']
})
export class UpdateProjectComponent implements OnInit {

  id!: string;

  constructor(private _projectService: ProjectService, private _router: Router, private _route: ActivatedRoute) { }

  ngOnInit(): void {
    this.id = this._route.snapshot.paramMap.get('id') as string;
    this._projectService.getById(this.id).subscribe((project) => {
      if (project) {
        this.myForm.patchValue({
          name: project.name,
          description: project.description
        });
      };
    });
  };

  myForm = new FormGroup({
    name: new FormControl('', [Validators.required]),
    description: new FormControl('', [Validators.required]),
  })

  update():void {    
    if (this.myForm.valid) {
      
      const values = this.myForm.value;
      const result = {...values} as unknown as Project;
      this._projectService.updateProject(result,this.id).subscribe(() => {
      
        const currentUrl = this._router.url;
        this._router.navigateByUrl('/', {skipLocationChange: true}).then(() => {
          this._router.navigate(['/']);
        });
      });
    }
  }

}
