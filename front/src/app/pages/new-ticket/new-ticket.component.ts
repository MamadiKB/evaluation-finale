import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { Ticket } from 'src/app/models/ticket';
import { TicketService } from 'src/app/service/ticket.service';

@Component({
  selector: 'app-new-ticket',
  templateUrl: './new-ticket.component.html',
  styleUrls: ['./new-ticket.component.css']
})
export class NewTicketComponent implements OnInit {
  
  id?: Number;
  
  constructor(private _ticketService: TicketService,private _router: Router, private _route: ActivatedRoute) { }

  ngOnInit(): void {
    this.id = Number(this._route.snapshot.paramMap.get('id'));
  }

  myForm = new FormGroup({
    title: new FormControl('', [Validators.required]),
    content: new FormControl('', [Validators.required]),
  })

  save():void {    
    if (this.myForm.valid) {
      
      const values = this.myForm.value 
      const result = {...values, projectId: this.id} as unknown as Ticket
      this._ticketService.addTicket(result).subscribe(() => {

        this._router.navigateByUrl('/', {skipLocationChange: true}).then(() => {
          this._router.navigate(['detail/' + this.id]);
        });
      })
    }
  }

}
