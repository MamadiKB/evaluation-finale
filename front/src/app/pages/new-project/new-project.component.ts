import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { ProjectService } from 'src/app/service/project.service';
import { Router } from '@angular/router';
import { Project } from 'src/app/models/project';

@Component({
  selector: 'app-new-project',
  templateUrl: './new-project.component.html',
  styleUrls: ['./new-project.component.css']
})
export class NewProjectComponent implements OnInit {

  constructor(private _projectService: ProjectService,private _router: Router) { }

  ngOnInit(): void {
  }

  myForm = new FormGroup({
    name: new FormControl('', [Validators.required]),
    description: new FormControl('', [Validators.required]),
  })

  save():void {    

    if (this.myForm.valid) {
      
      const values = this.myForm.value 
      const result = {...values} as unknown as Project
      this._projectService.addProject(result).subscribe(() => {

        this._router.navigateByUrl('/', {skipLocationChange: true}).then(() => {
          this._router.navigate(['/']);
        });
      })
    }
  }

}
