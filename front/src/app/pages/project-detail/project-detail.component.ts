import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Project } from 'src/app/models/project';
import { Ticket } from 'src/app/models/ticket';
import { ProjectService } from 'src/app/service/project.service';

@Component({
  selector: 'app-project-detail',
  templateUrl: './project-detail.component.html',
  styleUrls: ['./project-detail.component.css']
})
export class ProjectDetailComponent implements OnInit {

  id?: string;

  project?: Project;

  ticketTodo: Ticket[] = [];

  ticketInProgress: Ticket[] = [];

  ticketDone: Ticket[] = []

  constructor(private _projectService: ProjectService, private _router: Router, private _route: ActivatedRoute) { }

  ngOnInit(): void {
    this.id = this._route.snapshot.paramMap.get('id') as string;
    this._projectService.getById(this.id).subscribe( project => {
      this.project = project;

      project.ticketsList.map((ticket:Ticket) => {

        switch(ticket.status) {
          case 'TODO':
            this.ticketTodo.push(ticket);
            break;
          case 'INPROGRESS':
            this.ticketInProgress.push(ticket);
            break;
          case 'DONE':
              this.ticketDone.push(ticket);
            break;  
        }
      })
    })
  }

  deleteProject():void {
    this.id = this._route.snapshot.paramMap.get('id') as string;
    this._projectService.delete(this.id).subscribe(() => {
      
      this._router.navigateByUrl('/', {skipLocationChange: true}).then(() => {
        this._router.navigate(['/']);
      })
    });
  }

}
