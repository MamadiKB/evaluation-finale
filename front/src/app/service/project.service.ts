import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Project } from '../models/project';

@Injectable({
  providedIn: 'root'
})
export class ProjectService {

  API_LINK: string = "/api/project";

  constructor(private _httpClient: HttpClient) { }

  getAll(): Observable<Project[]> {
    return this._httpClient.get<Project[]>(this.API_LINK)
  }

  getById(id: string): Observable<Project> {
    return this._httpClient.get<Project>(this.API_LINK + '/' + id )
  }

  delete(id: string): Observable<Project> {
    return this._httpClient.delete<Project>(this.API_LINK + '/' + id)
  }

  addProject(project:Project): Observable<Project> {
    return this._httpClient.post<Project>(this.API_LINK, project)
  }

  updateProject(project:Project, id:string): Observable<Project> {
    return this._httpClient.put<Project>(this.API_LINK + '/' + id, project)
  }

}
