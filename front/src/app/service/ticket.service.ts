import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Ticket } from '../models/ticket';
import { StatusTicket } from '../models/status-ticket';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class TicketService {

  constructor(private _httpClient: HttpClient) { }

  API_LINK: string = "/api/ticket";

  addTicket(ticket:Ticket):Observable<Ticket> {
    return this._httpClient.post<Ticket>(this.API_LINK, ticket);
  };

  changeStat(status:StatusTicket, id:number):Observable<StatusTicket> {
    return this._httpClient.patch<StatusTicket>(this.API_LINK + '/status/' + id, status);
  };

  getTicketById(id:string):Observable<Ticket> {
    return this._httpClient.get<Ticket>(this.API_LINK + '/' + id);
  };

  updateTicket(ticket:Ticket,id:string):Observable<Ticket> {
    return this._httpClient.put<Ticket>(this.API_LINK + '/' + id, ticket);
  };

  deletTicket(id:string):Observable<Ticket> {
    return this._httpClient.delete<Ticket>(this.API_LINK + '/' + id);
  };
  
}
