import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import { AccueilComponent } from './pages/accueil/accueil.component';
import { ProjectDetailComponent } from './pages/project-detail/project-detail.component';
import { NewProjectComponent } from './pages/new-project/new-project.component';
import { NewTicketComponent } from './pages/new-ticket/new-ticket.component';
import { UpdateProjectComponent } from './pages/update-project/update-project.component';
import { UpdateTicketComponent } from './pages/update-ticket/update-ticket.component';
import { LoginComponent } from './components/login/login.component';

const routes: Routes = [
  { path: '', redirectTo: 'accueil',pathMatch: 'full',},
  { path: 'accueil', component: AccueilComponent },
  { path: 'accueil/project/add', component: NewProjectComponent },
  { path: 'detail/:id', component: ProjectDetailComponent },
  { path: 'detail/:id/ticket/add', component: NewTicketComponent },
  { path: 'detail/:projectId/ticket/update/:ticketId', component: UpdateTicketComponent },
  { path: 'detail/:id/project/update', component: UpdateProjectComponent },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {
}

