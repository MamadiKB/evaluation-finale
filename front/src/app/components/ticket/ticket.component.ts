import { Component, Input, OnInit } from '@angular/core';
import { Ticket } from '../../models/ticket';
import { TicketService } from 'src/app/service/ticket.service';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-ticket',
  templateUrl: './ticket.component.html',
  styleUrls: ['./ticket.component.css']
})
export class TicketComponent {
  
  @Input() ticketList:Ticket[] = []
  @Input() projectId!:string

  constructor(private _ticketService: TicketService, private _router: Router, private _route: ActivatedRoute) { }

  statProg(ticketId:string):void {
    this._ticketService.changeStat({status:"INPROGRESS"},Number(ticketId)).subscribe(() =>{
      
      const currentUrl = this._router.url;
      this._router.navigateByUrl('/', {skipLocationChange: true}).then(() => {
        this._router.navigate([currentUrl]);
      });
    })
  }

  statDone(ticketId:string):void {
    this._ticketService.changeStat({status:"DONE"},Number(ticketId)).subscribe(() =>{
      
      const currentUrl = this._router.url;
      this._router.navigateByUrl('/', {skipLocationChange: true}).then(() => {
        this._router.navigate([currentUrl]);
      });
    })
  }

  deletTicket(id:string):void {
    this._ticketService.deletTicket(id).subscribe(() => {

      const currentUrl = this._router.url;
      this._router.navigateByUrl('/', {skipLocationChange: true}).then(() => {
        this._router.navigate([currentUrl]);
      })
    });
  }

}
