package com.zenika.poei.zira.models.dto;

import java.time.Instant;

public record TicketDto (
        Integer id,
        String title,
        String content,
        Instant createdAt,
        String status,
        Integer projectId

) { }
