package com.zenika.poei.zira.models.dto;

public enum TicketStatus {
    TODO,
    INPROGRESS,
    DONE
}
