package com.zenika.poei.zira.repositorys;

import com.zenika.poei.zira.models.Project;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface ProjectRepository extends JpaRepository<Project, Integer> {
    List<Project> findByOrderByCreatedAtDesc();
}
