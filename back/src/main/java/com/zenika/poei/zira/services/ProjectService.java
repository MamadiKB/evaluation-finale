package com.zenika.poei.zira.services;


import com.zenika.poei.zira.models.Project;
import com.zenika.poei.zira.models.dto.ProjectDto;
import com.zenika.poei.zira.models.mappers.ProjectMapper;
import com.zenika.poei.zira.repositorys.ProjectRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class ProjectService {
    private static final Logger LOG = LoggerFactory.getLogger(ProjectService.class);
    private final ProjectRepository projectRepository;
    private final ProjectMapper projectMapper;

    public ProjectService(ProjectRepository projectRepository, ProjectMapper projectMapper) {
        this.projectRepository = projectRepository;
        this.projectMapper = projectMapper;
    }

    public Project getOneProject(Integer id) {
        Optional<Project> currentProject = projectRepository.findById(id);
        return currentProject.get();
    }

    public void createProject(ProjectDto projectDto) {
        Project newProject = projectMapper.toModel(projectDto);
        projectRepository.save(newProject);
    }

    public void updateProject(Integer id,ProjectDto projectDto) {

        Optional<Project> optionalProject = projectRepository.findById(id);

        if (optionalProject.isPresent()) {

            Project projectToUpdate = optionalProject.get();
            projectToUpdate.setName(projectDto.name());
            projectToUpdate.setDescription(projectDto.description());
            projectRepository.save(projectToUpdate);
        }
    }

    public void deleteProject(Integer id) {
        Optional<Project> optProject = projectRepository.findById(id);

        if (optProject.isPresent()) {
            Project projectToDelete = optProject.get();
            projectRepository.delete(projectToDelete);
        }
    }
}
