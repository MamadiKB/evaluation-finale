package com.zenika.poei.zira.Controllers;

import com.zenika.poei.zira.models.Project;
import com.zenika.poei.zira.models.dto.ProjectDto;
import com.zenika.poei.zira.repositorys.ProjectRepository;
import com.zenika.poei.zira.services.ProjectService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/project")
public class ProjectController {

    private final ProjectRepository projectRepository;
    private final ProjectService projectService;

    public ProjectController(ProjectRepository projectRepository,ProjectService projectService) {
        this.projectRepository = projectRepository;
        this.projectService = projectService;
    }

    /**
     * ProjectController :Methode for get all projects
     * @return ResponseEntity
     */
    @RequestMapping(method = RequestMethod.GET)
    public ResponseEntity<?> getAllProjects() {
        try {
            List<Project> projectsList = projectRepository.findByOrderByCreatedAtDesc();
            return ResponseEntity.ok(projectsList);
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("ERROR: "+ e);
        }
    }

    /**
     * ProjectController : Methode for get one project by id
     * @param id Integer : id of the Project
     * @return ResponseEntity
     */
    @RequestMapping(value = "/{id}" ,method = RequestMethod.GET)
    public ResponseEntity<?> getOnProject(@PathVariable Integer id) {
        try {
            Project projects = projectService.getOneProject(id);
            return ResponseEntity.ok(projects);
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("ERROR: "+ e);
        }
    }

    /**
     * ProjectController : Methode for create one project with no tickets
     * @param projectDto : dto of project model
     * @return ResponseEntity
     */
    @RequestMapping(method = RequestMethod.POST)
    public ResponseEntity<?> createProject(@RequestBody ProjectDto projectDto) {
        try {
            projectService.createProject(projectDto);
            return ResponseEntity.ok(HttpStatus.CREATED);
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("ERROR: "+ e);
        }
    }

    /**
     * ProjectController : Methode for update one project
     * @param id of the Project to update
     * @param projectDto : dto of project model
     * @return ResponseEntity
     */
    @RequestMapping(value = "/{id}",method = RequestMethod.PUT)
    public ResponseEntity<?> updateProject(@PathVariable Integer id, @RequestBody ProjectDto projectDto) {
        try {
            projectService.updateProject(id, projectDto);
            return ResponseEntity.ok(HttpStatus.ACCEPTED);
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("ERROR: "+ e);
        }
    }

    /**
     *  ProjectController : Methode for delete one project
     * @param id id of the Project to delete
     * @return ResponseEntity
     */
    @RequestMapping(value = "/{id}",method = RequestMethod.DELETE)
    public ResponseEntity<?> deleteProject(@PathVariable Integer id) {
        try {
            projectService.deleteProject(id);
            return ResponseEntity.ok(HttpStatus.ACCEPTED);
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("ERROR: "+ e);
        }
    }
}
