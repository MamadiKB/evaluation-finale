package com.zenika.poei.zira.models.mappers;

import com.zenika.poei.zira.models.Project;
import com.zenika.poei.zira.models.dto.ProjectDto;
import org.springframework.stereotype.Component;

@Component
public class ProjectMapper {

    public ProjectDto toDto(Project project) {
        ProjectDto projectDto = new ProjectDto(
                project.getId(),
                project.getName(),
                project.getDescription(),
                project.getCreatedAt(),
                project.getTicketsList()
        );
        return projectDto;
    }

    public Project toModel(ProjectDto projectDto) {
        Project project = new Project();
        project.setId(projectDto.id());
        project.setName(projectDto.name());
        project.setDescription(projectDto.description());
        project.setCreatedAt(projectDto.createdAt());
        project.setTicketsList(projectDto.ticketList());

        return project;
    }
}
