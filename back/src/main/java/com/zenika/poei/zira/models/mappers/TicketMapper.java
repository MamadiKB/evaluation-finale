package com.zenika.poei.zira.models.mappers;

import com.zenika.poei.zira.models.Project;
import com.zenika.poei.zira.models.Ticket;
import com.zenika.poei.zira.models.dto.StatusTicketDto;
import com.zenika.poei.zira.models.dto.TicketDto;
import com.zenika.poei.zira.repositorys.ProjectRepository;
import com.zenika.poei.zira.repositorys.TicketRepository;
import org.springframework.stereotype.Component;

import java.time.Instant;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;


@Component
public class TicketMapper {

    ProjectRepository projectRepository;
    TicketRepository ticketRepository;
    public TicketMapper(ProjectRepository projectRepository, TicketRepository ticketRepository) {
        this.projectRepository = projectRepository;
        this.ticketRepository = ticketRepository;
    }

    public TicketDto toDto(Ticket ticket) {
        TicketDto ticketDto = new TicketDto(
                ticket.getId(),
                ticket.getTitle(),
                ticket.getContent(),
                ticket.getCreatedAt(),
                ticket.getStatus(),
                ticket.getProjectId().getId()
        );
        return ticketDto;
    }

    public List<TicketDto> toDtoList(List<Ticket> tickets) {

        List<TicketDto> ticketDtoArray = new ArrayList<>();

        for (Ticket ticket : tickets) {
            TicketDto ticketDto = new TicketDto(
                    ticket.getId(),
                    ticket.getTitle(),
                    ticket.getContent(),
                    ticket.getCreatedAt(),
                    ticket.getStatus(),
                    ticket.getProjectId().getId()
            );
            ticketDtoArray.add(ticketDto);
        }
        return ticketDtoArray;
    }

    public Ticket toModel(TicketDto ticketDto) {
        Ticket ticket = new Ticket();
        ticket.setId(ticketDto.id());
        ticket.setTitle(ticketDto.title());
        ticket.setContent(ticketDto.content());
        ticket.setCreatedAt(Instant.now());

        if (ticketDto.status() != null) {
            ticket.setStatus(ticketDto.status());
        } else {
            ticket.setStatus("TODO");
        }
        //TODO Gestion d'optional
        Project currentProject = projectRepository.findById(ticketDto.projectId()).get();
        ticket.setProjectId(currentProject);

        return ticket;
    }

    public Ticket forStatus(Integer id, StatusTicketDto statusTicketDto) {
        Optional<Ticket> optTicket = ticketRepository.findById(id);
        //TODO Gestion d'optional
        Ticket ticket = optTicket.get();
        ticket.setStatus(statusTicketDto.status());
        return ticket;
    }

}
