package com.zenika.poei.zira.models.dto;

import com.zenika.poei.zira.models.Ticket;

import java.time.Instant;
import java.util.List;

public record ProjectDto (
        Integer id,
        String name,
        String description,
        Instant createdAt,
        List<Ticket> ticketList
) { }
