package com.zenika.poei.zira.repositorys;

import com.zenika.poei.zira.models.Ticket;
import org.springframework.data.jpa.repository.JpaRepository;

public interface TicketRepository extends JpaRepository<Ticket, Integer> {
}
