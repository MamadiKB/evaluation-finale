package com.zenika.poei.zira.models.dto;

public record StatusTicketDto(
        String status
) {}
