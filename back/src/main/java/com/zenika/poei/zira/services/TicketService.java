package com.zenika.poei.zira.services;


import com.zenika.poei.zira.models.Ticket;
import com.zenika.poei.zira.models.dto.StatusTicketDto;
import com.zenika.poei.zira.models.dto.TicketDto;
import com.zenika.poei.zira.models.mappers.TicketMapper;
import com.zenika.poei.zira.repositorys.TicketRepository;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class TicketService {
    private final TicketRepository ticketRepository;

    private final TicketMapper ticketMapper;
    public TicketService(TicketRepository ticketRepository, TicketMapper ticketMapper) {
        this.ticketRepository = ticketRepository;
        this.ticketMapper = ticketMapper;
    }

    public List<TicketDto> getAllTickets() {

        List<Ticket> tickets = ticketRepository.findAll();
        ticketMapper.toDtoList(tickets);
        return ticketMapper.toDtoList(tickets);
    }

    public void createTicket(TicketDto ticketDto) {

        Ticket newTicket = ticketMapper.toModel(ticketDto);
        ticketRepository.save(newTicket);
    }

    public void setTicketStatus(Integer id, StatusTicketDto statusTicketDto) {

        Ticket ticket = ticketMapper.forStatus(id,statusTicketDto);
        ticketRepository.save(ticket);
    }
    public void updateTicket(Integer id, TicketDto ticketDtoDto) {

        Optional<Ticket> optionalTicket = ticketRepository.findById(id);

        if (optionalTicket.isPresent()) {
            Ticket ticketToUpdate = optionalTicket.get();
            ticketToUpdate.setTitle(ticketDtoDto.title());
            ticketToUpdate.setContent(ticketDtoDto.content());
            ticketToUpdate.setStatus(ticketDtoDto.status());
            ticketRepository.save(ticketToUpdate);
        }
    }
}
