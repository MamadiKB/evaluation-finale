package com.zenika.poei.zira.Controllers;

import com.zenika.poei.zira.models.Ticket;
import com.zenika.poei.zira.models.dto.StatusTicketDto;
import com.zenika.poei.zira.models.dto.TicketDto;
import com.zenika.poei.zira.repositorys.TicketRepository;
import com.zenika.poei.zira.services.TicketService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/api/ticket")
//@SecurityRequirement(name = "basicAuth")
public class TicketController {

    private final TicketRepository ticketRepository;
    private final TicketService ticketService;

    public TicketController(TicketRepository ticketRepository, TicketService ticketService) {
        this.ticketRepository = ticketRepository;
        this.ticketService = ticketService;
    }

    /**
     * TicketController :Methode for get all tickets
     * @return ResponseEntity
     */
    @RequestMapping(method = RequestMethod.GET)
    public ResponseEntity<?> getAllTickets() {
        try {
            List<TicketDto> TicketsList = ticketService.getAllTickets();
            return ResponseEntity.ok(TicketsList);
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("ERROR: "+ e);
        }
    }

    /**
     * TicketController : Methode for get one ticket by id
     * @param id Integer : id of the ticket
     * @return ResponseEntity
     */
    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    public ResponseEntity<?> getOneTicket(@PathVariable Integer id) {
        try {
            return ResponseEntity.ok(ticketRepository.findById(id));
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("ERROR: "+ e);
        }

    }

    /**
     * TicketController : Methode for create one tickets
     * @param ticketDto : dto of ticket model, contain project id
     * @return ResponseEntity
     */
    @RequestMapping(method = RequestMethod.POST)
    public ResponseEntity<?> createTicket(@RequestBody TicketDto ticketDto) {
        try {
            ticketService.createTicket(ticketDto);
            return ResponseEntity.ok(HttpStatus.ACCEPTED);
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("ERROR: "+ e);
        }
    }

    /**
     * TicketController : Methode for change the statut of a ticket
     * @param id of the Project to ticket
     * @param statusTicketDto : dto for statut of a ticket
     * @return ResponseEntity
     */
    @RequestMapping(value = "status/{id}", method = RequestMethod.PATCH)
    public ResponseEntity<?> setTicketStatus(@PathVariable Integer id, @RequestBody StatusTicketDto statusTicketDto) {
        try {
            ticketService.setTicketStatus(id,statusTicketDto);
            return ResponseEntity.ok(HttpStatus.ACCEPTED);
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("ERROR: "+ e);
        }
    }

    /**
     * TicketController : Methode for update one ticket
     * @param id of the Project to ticket
     * @param ticketDto : dto of project ticket
     * @return ResponseEntity
     */
    @RequestMapping(value = "/{id}",method = RequestMethod.PUT)
    public ResponseEntity<?> updateTicket(@PathVariable Integer id, @RequestBody TicketDto ticketDto) {
        try {
            ticketService.updateTicket(id, ticketDto);
            return ResponseEntity.ok(HttpStatus.ACCEPTED);
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("ERROR: "+ e);
        }
    }

    /**
     * TicketController : Methode for delete one ticket
     * @param id id of the Ticket to delete
     * @return ResponseEntity
     */
    @RequestMapping(value = "/{id}",method = RequestMethod.DELETE)
    public ResponseEntity<?> deleteTicket(@PathVariable Integer id) {

        Optional<Ticket> optTicket = ticketRepository.findById(id);
        if (optTicket.isPresent()) {
            Ticket projectTicket = optTicket.get();
            ticketRepository.delete(projectTicket);
            return ResponseEntity.ok(HttpStatus.ACCEPTED);
        }
        return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("ticket not find");
    }
}
