--- project
INSERT INTO project (name, description)
VALUES ('zira', 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat massa quis enim.')
ON CONFLICT DO NOTHING;
INSERT INTO project (name, description)
VALUES ('StarShip', 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean m')
ON CONFLICT DO NOTHING;


INSERT INTO ticket (title, content, project_id)
VALUES ('back',  'Lorem ipsum dolor sit amet, co', 1)
ON CONFLICT DO NOTHING;
INSERT INTO ticket (title, content, project_id)
VALUES ('Front',  'Lorem ipsum dolor sit amet, co', 1)
ON CONFLICT DO NOTHING;
INSERT INTO ticket (title, content, project_id)
VALUES ('build',  'Lorem ipsum dolor sit amet, co', 1)
ON CONFLICT DO NOTHING;

INSERT INTO ticket (title, content, project_id)
VALUES ('Elon', 'Lorem ipsum dolor sit amet, co', 2)
ON CONFLICT DO NOTHING;
INSERT INTO ticket (title, content, project_id)
VALUES ('crash', 'Lorem ipsum dolor sit amet, co', 2)
ON CONFLICT DO NOTHING;
INSERT INTO ticket (title, content, project_id)
VALUES ('Repair', 'Lorem ipsum dolor sit amet, co', 2)
ON CONFLICT DO NOTHING;

