#!/usr/bin/env bash

set -e

based_tag_name=${CI_REGISTRY_IMAGE:-mamadi.kaba/zira}

mvn clean package
docker build -t "$based_tag_name/back:latest" .